FROM python:3-alpine

# Note that everything is uninstalled later.
ADD requirements.txt /requirements.txt

RUN apk add --no-cache libpq postgresql-dev git build-base libressl-dev libffi-dev libxslt libxml2-dev libxslt-dev && \
  pip install -U -r /requirements.txt && \
  apk del postgresql-dev git build-base libressl-dev libffi-dev libxml2-dev libxslt-dev

ADD . /app
RUN pip install -e /app

# Bridge database (registrations, chatrooms, ...)
VOLUME /root/.local/share/programaker/bridges/caldav-bridge/db.sqlite

WORKDIR /app

CMD programaker-caldav-bridge
