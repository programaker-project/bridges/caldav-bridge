import datetime

import caldav


class CalEvent:
    def __init__(self, event: caldav.objects.Event):
        self.data = event
        self.cal = event.icalendar_instance

    def get(self, prop):
        for sc in self.cal.subcomponents:
            val = sc.get(prop)
            if val is not None:
                return val
        return None

    @property
    def summary(self):
        return str(self.get("SUMMARY"))

    @property
    def trueStartTime(self):
        time = self.get("DTSTART").dt
        if isinstance(time, datetime.datetime):
            return time
        else:
            return datetime.datetime.combine(
                time, datetime.time(0, 0), tzinfo=datetime.timezone.utc
            )

    @property
    def id(self):
        return self.data.id

    @property
    def url(self):
        return self.data.canonical_url
