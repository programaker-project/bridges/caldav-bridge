from setuptools import setup

setup(
    name="programaker-caldav-bridge",
    version="0.1",
    description="Programaker service to interact with a CalDAV calendar.",
    author="kenkeiras",
    author_email="kenkeiras@codigoparallevar.com",
    license="Apache License 2.0",
    packages=["programaker_caldav_bridge"],
    include_package_data=True,
    scripts=["bin/programaker-caldav-bridge"],
    install_requires=[
        "caldav",
        "icalendar",
        "programaker-bridge[storage]",
        "sqlalchemy",
        "xdg",
    ],
    zip_safe=False,
)
